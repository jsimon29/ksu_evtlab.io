---
layout: post
title: Welcome To EVT 2019
tags: []
author-id: mdavenport
hide_title: false
---
Hello, my name is Matthew Davenport and I am the newly elected president of the Kennesaw State Electric Vehicle Team (KSU EVT). This season we hope to achieve something that will impact the future of electric cars. Before I get into what is planned for this season, I would like to recap this past season. This past season we designed and built a prototype electric autonomous go-kart and took part in building an electric formula SAE racecar. At our competition, Electric Vehicle Grand Prix in Indianapolis, Indiana, we placed 1st overall, 1st in design, and 1st in energy efficiency for our driven electric go-kart, and 3rd in the race for the autonomous go-kart.

This upcoming season, we have the goal of growing and sustaining a team with an active membership of a larger scale than ever before.  For these new and current members, we have the two major projects being an autonomous go-kart and a racing team surrounding our electric go-kart. For the autonomous go-kart project, this season is [working] toward finishing the autonomous race at the evgrandprix competition as a learning experience to grow into a larger competition such as the SAE Autodrive competition which competes with a full car.[ Our racing division will be focused on maintaining our competitive edge in the evgrandprix main race competition and an outreach presence focused around racing electric go-karts. One example is having a more pronounced presence at the go-kart race track in Barnseville, GA, of which our team was the first electric go-kart to race on the track in the open-series race.] In addition to this, we will be getting in contact with other colleges to try to influence them into creating their own electric go-kart to try to establish a new kart series at the track.

The leadership of EVT this summer has been hard at work developing a better team structure and evaluating new improvements to grow a dedicated and well-rounded team.  We are fully dedicated to serving our members this season to give everyone the best possible experience in taking their interest and passions and translating them into award winning engineering projects. This year will establish a foundation for KSU EVT to be successful more than ever before.
