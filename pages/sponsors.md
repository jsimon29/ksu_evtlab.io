---
layout: page
title: Sponsors
permalink: /sponsors/
feature-img: "assets/img/teams/autonomous_banner.jpg"
tags: [Sponsors, Archive]
pagination: 
  enabled: true
---

In order to be able to compete in such competitions and events as Purdue’s EvGrandprix driverless and driven race, a large amount of financial and material support is needed. Without sponsorship it would neither be possible for us to develop and manufacture our designs nor compete in our events.

Therefore, we would like to thank all our sponsors for their generous support!

![Precision Techwood Enterprises](/assets/img/sponsor/techwood.png)
![Velodyne](/assets/img/sponsor/velodyne.png)
![KSU SABAC](/assets/img/sponsor/ksu.png)

