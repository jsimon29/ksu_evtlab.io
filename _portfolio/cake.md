---
layout: page
title: Autonomous Division
feature-img: "assets/img/portfolio/cake.png"
img: "assets/img/portfolio/cake.png"
date: 27 September 2015
tags: [Lorem, Ipsum, Portfolio]
---
This year, the autonomous division of EVT is a new part of the organization. This year the team has the goal of taking first place in the autonomous division at the EvGrandPrix competition (https://evgrandprix.org/autonomous/) during mid-May of 2020. Our goals are three fold: 1. Ground-up re-design of software architecture built around Ros2 and B-Spline SLAM, 2. Be built around safety and reliability, and 3. Achieve a thoroughly tested and reliable system. The team has its own Velodyne VLP-16 lidar, as well as two smaller lidar sensors being a YDlidar and an RPlidar which are used on a small-scale test platform. In order to develop the software architecture the team will be using a gazebo simulator to do full and partial stack testing with model-based motion control and fully simulated sensor input. 
