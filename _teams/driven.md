---
layout: page
title: Driven Division
feature-img: "assets/img/teams/driven_banner.jpg"
img: "assets/img/teams/driven_icon.jpg"
date: 27 September 2015
tags: [Lorem, Ipsum, Teams]
---
The driven Go-Kart team works towards understanding electric drivetrains, circuits, and the entire manufacturing process in order to build the best kart possible. Since 2013, EVT has competed in the collegiate competition at [EvGrandPrix](https://evgrandprix.org/collegiate/) taking first place overall twice in 2016 and 2019.  This year, the team aims at improving the existing design with a focus on battery efficiency, dash design and overall system stability. Each May the team travels to the Indianapolis Motor Speedway to race against over thirty other teams wheel-to-wheel. The kart is tested in speed, endurance, energy efficiency, and design. 


